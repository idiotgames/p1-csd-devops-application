FROM php:7-fpm
RUN apt-get update && \
    apt-get install -y \
      git
RUN apt-get install -y zlib1g-dev \
    && docker-php-ext-install zip

RUN apt-get install -y libmemcached-dev \
    && curl -L -o /tmp/memcached.tar.gz "https://github.com/php-memcached-dev/php-memcached/archive/php7.tar.gz" \
    && mkdir -p /usr/src/php/ext/memcached \
    && tar -C /usr/src/php/ext/memcached -zxvf /tmp/memcached.tar.gz --strip 1 \
    && docker-php-ext-configure memcached \
    && docker-php-ext-install memcached \
    && rm /tmp/memcached.tar.gz

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/bin/ --filename=composer
RUN mkdir -p /var/www/composer/vendor
RUN mkdir -p /var/www/html/reports/
ENV PATH "/var/www/html/vendor/bin:$PATH"

WORKDIR /var/www/html/

# add our composer dependancies
COPY composer.* ./

# install our dependancies without running scripts
# then remove composers cache to save space
RUN composer install --no-scripts --no-autoloader --prefer-dist -vvv

RUN mkdir -p /mnt/stats && chmod 777 -R /mnt/stats

COPY ./src/ /var/www/html/
RUN composer dump-autoload --optimize
