Since build.sh script is not working, guide to windows installation:
- create docker-compose.yml
- copy content from docker-compose-template.yml to docker-compose.yml
- replace APP_VERSION and NGINX_VERSION in docker-compose.yml with values from env.sh
- run docker-compose up
- if mounting volumes does not work, comment out the volumes in docker-compose.override.yml