https://join.slack.com/t/p1-workshop/shared_invite/enQtMzU1ODcwNTEyNDY5LTA1ODA4YzFhYTE0MGJmNWQ3MGU0YjYxMzJmMzI5OGI3NmEzNzU2NTI0NWZiOGNmNTkzYTU5MWE4OTRkYmJiZjg

https://bitbucket.org/paragrapheins/p1-csd-devops-application/src/master/

Running on mac:

1. Install *envsubst*  
*brew install gettext*  
*brew link --force gettext*
2. run docker
3. run docker monitor  
*docker network create monitoring*
4. build project  
*./build.sh*
5. run project  
*docker-compose up*  
6. see what you have  
*http://localhost:8000/*

Build:

* docker build -f build/Dockerfile --tag csd-build .
* docker run -v $(pwd):/mnt -v /var/run/docker.sock:/var/run/docker.sock csd-build
