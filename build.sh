source ./env.sh && \
rm -rf docker-compose.yml &&\
envsubst < "docker-compose-template.yml" > "docker-compose.yml" && \
docker-compose build
mkdir -p dist
docker-compose -f docker-compose.yml -f docker-compose.prod.yml config > dist/stack.prod.yml
docker-compose -f docker-compose.yml -f docker-compose.stage.yml config > dist/stack.stage.yml
