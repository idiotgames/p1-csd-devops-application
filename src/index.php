<?php
declare(strict_types=1);

if ($_SERVER['REQUEST_URI'] !== '/') {
    header("HTTP/1.0 404 Not Found");
    exit;
}

function incrementAndReturnGlobalCounter(string $key): int
{
    $memcache_obj = new Memcached();
    $memcache_obj->addServer('memcached', 11211);

    $counter = (int)$memcache_obj->get($key);

    $memcache_obj->set($key, ++$counter);

    return $counter;
}

function addIWasHereAndGet($name)
{
    $memcache_obj = new Memcached();
    $memcache_obj->addServer('memcached', 11211);

    $names = $memcache_obj->get('names');

    if ($names) {
        $names = unserialize($names);
    }

    if (!$names) {
        $names = [];
    }

    if ($name) {
        $names[] = [$name, new \DateTime()];

        $memcache_obj->set('names', serialize($names));
    }

    return $names;
}

$names = addIWasHereAndGet($_POST['name']);

$localCounter = 'localCounter-' . getenv('HOSTNAME');
$globalCounterKey = 'globalCounter';

echo "Hallo, welcome to DevOps container. Das ist ja toll!<br>";
echo "I am running in <strong>" . getenv("ENVIRONMENT") . "</strong> mode <br><br>";

echo "Container Hostname: " . getenv('HOSTNAME') . "<br><br>";

echo "<form method='post'>";
echo "<input type='text' name='name'>";
echo "</form>";

echo "Container Counter: " . incrementAndReturnGlobalCounter($localCounter) . "<br><br>";

echo "Global Counter: " . incrementAndReturnGlobalCounter($globalCounterKey) . "<br><br>";

foreach ($names as list($name, $date)) {
    echo htmlspecialchars($name) . ' was here on ' . $date->format('Y-m-d H:i:s') . '<br>';
}
